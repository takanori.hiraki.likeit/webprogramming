package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserAdd
 */
@WebServlet("/UserAdd")
public class UserAdd extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserAdd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		/**** ログインの有無確認 start ****/
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("userInfo");

		// ログイン情報がない場合
		if (user == null) {
			// セッションがなかったらログイン画面へ
			response.sendRedirect("LoginServlet");
			return;
		}
		/**** ログインの有無確認 end ****/
		// 新規登録のjspにフォワード
				RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
				dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		UserDao userDao = new UserDao();

		// 新規登録画面で入力された情報を取得
		String loginId = request.getParameter("user-loginid");
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("password-confirm");
		String name = request.getParameter("user-name");
		String birthdate = request.getParameter("birth-date");
		
		/**** 新規登録画面に未入力があった or パスワードが違う or login_idが既に登録されている 場合 ****/
		if (!isInputAll(loginId, password, confirmPassword, name, birthdate) || !(password.equals(confirmPassword))
				|| userDao.existUser(loginId)) {
			// CHECK: 以下のように条件式が長くなる場合はメソッド抽出すると見やすくなる
			// 詳しい解説はisInputAllメソッド内に記述
			// if (loginId.equals("") || password.equals("") || confirmPassword.equals("")
			// ||
			// name.equals("")
			// || birthdate.equals("") ・・・) {

			// エラーメッセージ
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// 入力内容を画面に表示するために、リクエストスコープに値をセットしておく
			request.setAttribute("inputloginId", loginId);
			request.setAttribute("name", name);
			request.setAttribute("birthdate", birthdate);

			// 新規登録画面へ遷移
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);
			return;
		}
		

		/**** 新規登録成功時 ****/
		

		// 登録処理
		userDao.insert(loginId, name, birthdate, password);
		response.sendRedirect("UserListServlet");
	}

		/**
		 * 新規登録画面の入力された値が全て入力されているかどうか
		 *
		 * @param loginId
		 * @param password
		 * @param confirmPassword
		 * @param name
		 * @param birthdate
		 * @return
		 */
		private static boolean isInputAll(String loginId, String password, String confirmPassword, String name,
				String birthdate) {
			// CHECK: メソッド・変数の命名について

			// メソッド名を決める際、戻り値の型がbooleanであれば「is,has,can,exist」から始めるというルールがある
			// PersonクラスのisManメソッドがtrueだったとき、Personは男でしょぅか？女でしょうか？
			// 答えは男です。簡単な英語で理解できます。
			// メソッドや変数の命名は、このように簡単に理解できるようにつけます。

			// 例えば、PersonクラスにisNotWomanメソッドがあったとしたらどうでしょう。
			// そしてそれが条件式で以下のように呼び出されていたらどうでしょう。

			// if (!person.isNotMan())
			// この条件分岐は実行されるかされないか、ぱっと理解できますか？
			// ※ !がついてたら「○○じゃない時」って理解すればいいです。personが男性ではないじゃない時？男性なのか？女性なのか？？理解しにくくなります。

			// 命名には最新の注意を払って下さい。バグを生みます。

			return !(loginId.equals("") || password.equals("") || confirmPassword.equals("") || name.equals("")
					|| birthdate.equals(""));

			// CHECK: メソッド抽出について
			// メソッドの強力なメリットとして、処理に名前をつけて可読性を上げる事ができる、というものがあります。
			// 上記の条件だけでは各変数が空文字と比較してるだけで、意図は読めません。
			// でもメソッドにして名前をつけたら「入力された値があるかどうかチェックしてるんだな」と意図を伝えられます。
			// 正しい名前をつけて可読性の高いコードを書くことで、
			// 他者がコードを読む時間を減らし、修正や機能追加の際に迷ったり調べる時間が減るので開発効率が上がります。
			// 「他者」は他の開発者だけでなく、未来の自分も含まれます。（書いたコードはすぐ忘れてしまうものです）
			// チーム全体のために、動くのは当然として、読みやすいコードを意識して書きましょう。


	}

}
