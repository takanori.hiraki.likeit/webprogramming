CREATE DATABASE usermanagement CHARACTER SET utf8;


CREATE TABLE `user`(
    id serial PRIMARY KEY AUTO_INCREMENT NOT null,
    login_id varchar(255) UNIQUE NOT null,
    name varchar(255) NOT null,
    birth_date DATE NOT null,
    password varchar(255) NOT null,
    is_admin boolean NOT NULL DEFAULT false,
    create_date DATETIME NOT null,
    update_date DATETIME NOT null
);


INSERT INTO `user` (
	 id ,
 login_id ,
  name ,
  birth_date ,
  password  ,
  	is_admin,
    create_date ,
    update_date 
)
VALUES('admin',
       '管理者', 
       2000-06-12, 
       'password', 
       true, 
       now(), 
       now());

       